# Docker exporter

Monitor a proxy that is used to get requests every X seconds

## Requirements

- [Node.js](https://nodejs.org/)
- [yarn](https://yarnpkg.com/)

## Installation

```
# Install all the dependencies
yarn
```

## Configuration

Copy the file .env.example to .env and then edit it with your values :

```
cp .env.example .env
```
