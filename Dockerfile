FROM node:16-alpine

ENV NODE_ENV=production
EXPOSE 3000
WORKDIR /srv

COPY package.json yarn.lock ./

RUN yarn --frozen-lockfile

COPY ./ ./

RUN yarn build

CMD yarn start